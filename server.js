var express = require('express'),
	proxy = require('express-http-proxy'),
    app = express();

var apiProxy = proxy('http://reporter.dev/app_dev.php/', {
    forwardPath: function (req, res) {
    	console.log(req.baseUrl);
        return require('url').parse(req.baseUrl).path;
    }
});

app.use(express.static('www'));
app.use('/api', proxy('smart-service-reporter-ws.herokuapp.com'));


// CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// API Routes
// app.get('/blah', routeHandler);

app.set('port', process.env.PORT || 5000);

app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
