function Reporter(){
  this.customer = undefined;
  this.job = undefined;
  this.tasks = undefined;
}


// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
  'ionic',
  'ionic-material',
  'ionMdInput',
  'starter.controllers',
])
.factory('Config', function(){
  return {
    name: 'Customer Service Reporter'
  }
})
.factory('reporter', function(){
  return new Reporter();
})
.run(function($ionicPlatform, $http, $rootScope, Config) {
  $rootScope.config = Config
  $ionicPlatform.ready(function() {
    $http
    .get('/api/companies')
    .then(function(response){
      $rootScope.company = response.data[0];
    });

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'content': {
        templateUrl: 'templates/home.html',
        controller: 'UserCtrl'
      },
      'fabContent': {
          templateUrl: 'templates/common/fab.html',
          controller: function ($timeout) {
              $timeout(function () {
                  document.getElementById('fab-activity').classList.toggle('on');
              }, 200);
          }
      }
    }
  })

  .state('app.dashboard',{
    url: '/dashboard',
    views: {
      'content':{
        templateUrl: 'templates/dashboard.html',
        controller: 'UserCtrl'
      },
      'dashboard-tab': {
        templateUrl: 'templates/profile.html',
      }
    }
  })

  .state('app.profile', {
    url: '/profile',
    views: {
      'content': {
        templateUrl: 'templates/profile.html',
      }
    }
  })

  .state('app.settings', {
    url: '/settings',
    views: {
      content: {
        templateUrl: 'templates/settings.html'
      }
    }
  })

  .state('customer', {
    url: '/customer',
    abstract: true,
    templateUrl: 'templates/common/base.html',
    controller: 'CustomerCtrl'
  })

  .state('customer.create',{
    url: '/create',
    views: {
      'content':{
        templateUrl: 'templates/customer/create.html',
        controller: 'CustomerCtrl'
      }
    }
  })

  .state('customer.signature', {
    url: '/signature',
    views: {
      content: {
        templateUrl: 'templates/signature.html',
        controller: 'SignatureCtrl'
      }
    }
  })

  .state('job',{
    url: '/job',
    abstract: true,
    templateUrl: 'templates/common/base.html',
    controller: 'JobCtrl'
  })

  .state('job.create',{
    url: '/create',
    views: {
      'content': {
        templateUrl: 'templates/job/create.html',
        controller: 'JobCtrl'
      }
    }
  })

  .state('task', {
    url: '/task',
    abstract: true,
    templateUrl: 'templates/common/base.html',
    controller: 'TaskCtrl'
  })

  .state('task.list', {
    url: '/list',
    views: {
      content: {
        templateUrl: 'templates/task/list.html',
        controller: 'TaskCtrl'
      }
    }
  })

  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
