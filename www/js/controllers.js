angular.module('starter.controllers', [])

//.service('')

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $state, $location, $controller) {

  $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
    $scope.series = ['Series A', 'Series B'];
    $scope.data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
    ];

  $controller('JobCtrl', { $scope: $scope.$new() });
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  // $scope.$on('$ionicView.enter', function(e) {
  // });
})

.controller('UserCtrl', function($scope, $stateParams, $ionicModal) {
    // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    //$location.path('/app/dashboard');
    $state.go('app.dashboard');

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('CustomerCtrl', function($scope, $state, $http, reporter){
  $scope.create = function(){
    console.log('new Customer');

    //$scope.modal.show();
  }

  $scope.save = function(customer){
    $scope.customer = angular.copy(customer);
    reporter.customer = customer;
    $http
      .post('/api/customers', customer)
      .then(function(response){
        reporter.customer = response.data;
        $state.go('job.create');
      })
    ;
  }
})

.controller('JobCtrl', function($scope, $rootScope, $http, $location, reporter){

  if(!$scope.jobTypes){
    $http
      .get('/api/jobtypes')
      .then(function(response){
        jobTypes = response.data;
        for (var i = jobTypes.length - 1; i >= 0; i--) {
          jobTypes[i].selected = false;
        }
        $scope.jobTypes = jobTypes;
    });
  }

  $scope.save = function(jobTypes){
    $scope.jobTypes = angular.copy(jobTypes);
    reporter.jobTypes = jobTypes;
    console.log(jobTypes);
    console.log('creating job');
    $http
      .post('/api/jobs', jobTypes)
    $location.path('/task/list');
  }

  $scope.update = function(){

  }

  $scope.doUpdate = function(){
  }
})

.controller('TaskCtrl', function($scope, $state, $http, reporter){
  if(!$scope.tasks){
    $http
      .get('/api/tasks')
      .then(function(response){
        tasks = response.data;
        for (var i = tasks.length - 1; i >= 0; i--) {
          tasks[i].selected = false;
        }
        $scope.tasks = tasks;
    });
  }

  $scope.list = function(){
    console.log('run?');
  }

  $scope.save = function(tasks){
    console.log(tasks);
    reporter.tasks = new Array();
    for (var i = tasks.length - 1; i >= 0; i--) {
      if(tasks[i].selected){
        reporter.tasks.push(tasks[i]);
      }
    }
    $state.go('customer.signature');
  }

})

.controller('SignatureCtrl', function($scope){
  var canvas = document.getElementById('signatureCanvas');
  var signaturePad = new SignaturePad(canvas);

  $scope.clearCanvas = function() {
    signaturePad.clear();
  }

  $scope.saveCanvas = function() {
    var sigImg = signaturePad.toDataURL();
    $scope.signature = sigImg;
  }
})