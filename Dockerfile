FROM tobitheo/ionic-build-android-ci-docker:latest

RUN apt-get update
RUN apt-get install git openssh-client -y
RUN eval $(ssh-agent -s)
RUN mkdir -p ~/.ssh
RUN echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config


RUN echo -e '#!/bin/bash\n\necho "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa\necho "$SSH_PUBLIC_KEY" > ~/.ssh/id_rsa.pub\nchmod 400 ~/.ssh/id_rsa\nchmod 400 ~/.ssh/id_rsa.pub\nssh-add -l | grep "The agent has no identities" && ssh-add\nmkdir /project && cd /project\ngit clone git@gitlab.com:voidify/service-reporter-ui.git\nnpm install' >> /tmp/project.sh

RUN chmod +x /tmp/project.sh